###### 玩转金刚梯>金刚字典>

### 获取SSL型客户端
- SSL客户端软件系筑波大学作品，后台支撑由金刚公司提供
- 在Windows+PC环境下，点击此处[ 阅读配置说明并下载SSL型客户端 ]()
- SSL客户端软件须与[c9开头金刚号](/LadderFree/kkDictionary/KKIDWithC9Get.md)配套使用的

#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)

