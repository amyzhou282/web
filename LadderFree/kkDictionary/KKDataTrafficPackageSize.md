###### 玩转金刚梯>金刚字典>
### 流量包尺寸

- [ 金刚号梯 ](/kkproducts1.0.md)的[ 流量包 ](/kkdatatrafficpackage.md)尺寸共有15种，其中：
  - 免费[ 流量包 ](/kkdatatrafficpackage.md)[ 共3种尺寸 ](/kkdatatrafficfree.md)
  - 收费[ 流量包 ](/kkdatatrafficpackage.md)[ 共12种尺寸 ](/kkpriceofkkvpn1.0.md)
- [ 金刚App梯 ](/kkproducts2.0.md)的[ 流量包 ](/kkdatatrafficpackage.md)尺寸待定


#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)
